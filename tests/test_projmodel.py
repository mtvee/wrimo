import unittest
import json

import env
#import wrimo
from wrimo import projitem

# see http://johnnado.com/pyqt-qtest-example/
# and https://bitbucket.org/jmcgeheeiv/pyqttestexample

class ProjectModelTests(unittest.TestCase):
    
    def setUp( self ):
        pass
    
    def tearDown( self ):
        pass
    
    def setDefaults( self ):
        """
        Test Tree structure:
            Root
            |___ C01
            | |___ C11
            | |___ C111
            | |___ C111
            |___ C02
            |___ C03
            | |___ C31
        """        
        self.root = projitem.ProjItem('Root')
        self.child01 = projitem.ProjItem('C01')
        self.child02 = projitem.ProjItem('C02')
        self.child03 = projitem.ProjItem('C03')
        self.child11 = projitem.ProjItem('C11')
        self.child31 = projitem.ProjItem('C31')
        self.child111 = projitem.ProjItem('C111')
        self.child112 = projitem.ProjItem('C111')
        self.root.addChildren([self.child01, self.child02, self.child03])
        self.child01.addChild(self.child11)
        self.child03.addChild(self.child31)
        self.child11.addChild(self.child111)
        self.child11.addChild(self.child112)
    
    def test_model( self ):
        '''one should always equal one'''
        self.assertEqual( 1, 1 )
        
    def test_move( self ):
        self.setDefaults()
        self.assertEqual( len(self.child112), 0 )
        self.root.moveChild( self.child03, self.child112 )
        self.assertEqual( len(self.child112), 1 )
        #self.root.printPretty()

    def test_find( self ):
        self.setDefaults()        
        res = self.root.findItems('C31')
        self.assertEqual( len(res), 1 )
        self.assertEqual(res[0].getPath(),'Root.C03.C31')
        
        res = self.root.findItems('C111')
        self.assertEqual( len(res), 2 )
        self.assertEqual(res[0].getPath(),'Root.C01.C11.C111')
        self.assertEqual(res[1].getPath(),'Root.C01.C11.C111')

        
    def test_remove( self ):
        self.setDefaults()    
        # we find 2 of these
        res = self.root.findItems('C111')
        self.assertEqual( len(res), 2 )
        # remove the first one
        self.root.delItem( res[0].id )
        # now there is one left
        res = self.root.findItems('C111')
        self.assertEqual( len(res), 1 )
        
        # we find one toplevel
        res = self.root.findItems('C01')
        self.assertEqual( len(res), 1 )
        # and remove it
        self.root.delItem( res[0].id )
        # nothing should be left in that branch
        res = self.root.findItems('C111')
        self.assertEqual( len(res), 0 )


    def test_dict( self ):
        self.setDefaults()
        d1 = self.root.toDict()
        t1 = self.root.fromDict(d1)
        res = t1.findItems('C111')
        self.assertEqual( len(res), 2 )
        self.assertEqual(res[0].getPath(),'Root.C01.C11.C111')
        self.assertEqual(res[1].getPath(),'Root.C01.C11.C111')
