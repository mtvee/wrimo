# wrimo's setup.py
from distutils.core import setup

exec(open('wrimo/version.py').read())

setup(
    name         = 'Wrimo',
    version      = __version__,
    description  = 'A writing management tool',
    author       = 'mtvee',
    author_email = 'emptyvee@gmail.com',
    url          = 'http://bitbucket.com/mtvee/wrimo',
    download_url = 'http://bitbucket.com/mtvee/wrimo/dowload/something.tar.gz',
    packages     = ['wrimo'], # find_packages(),
    scripts      =['wrimo.py'],
    entry_points = {
        'console_scripts': [
            'wrimo = wrimo:main'
        ]
    },
    classifiers  = [
        'Programming Language :: Python 3',
        'Development Status :: 3 - Alpha',
        'Environment :: Other Environment',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Topic :: Text Processing',
        'Topic :: Text Editors'
    ],
    long_description = """\
Blah Blah
"""
)