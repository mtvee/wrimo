# Wrimo #

Wrimo is a writing project manager.

### Current ###

* Python version 3.4
* PyQt version 5.3.1
* Tested on: Windows 8.1

### Installation ###

* Requires: Python 3.4, PyQt 5.3.1
* [freezing](https://us.pycon.org/2012/schedule/presentation/393/)

### Contribution guidelines ###

* Writing tests
* Running tests
* Code review
* Other guidelines
    - use [napoleon](http://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html) style docstrings

### Who do I talk to? ###

* mtvee


### Attribution
[doc icons](https://github.com/teambox/Free-file-icons)