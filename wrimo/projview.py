import os
import json
import zipfile
import time
from PyQt5 import QtGui, QtCore, QtWidgets

from .projmodel import ProjModel

class ProjectTree(QtWidgets.QTreeView):
    """This class handles the project info
    """
    TYPE_DRAFT = QtWidgets.QTreeWidgetItem.UserType
    TYPE_TRASH = TYPE_DRAFT + 1
    PROJ_FILENAME = 'project.wrimo'
    
    def __init__(self, parent):
        super(ProjectTree, self).__init__(parent)
        self.model = ProjModel('Tester','.')
        self.setModel( self.model )
        self.projPath = None
        self.openProject()
        
        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        
        # context menu
        self.ctx_actions = {}
        self.ctx_actions['new'] = QtWidgets.QAction(QtGui.QIcon(':icons/document-new.png'),'New', self)
        self.ctx_actions['new'].triggered.connect(self.addItem)
        self.ctx_actions['import'] = QtWidgets.QAction(QtGui.QIcon(':icons/edit-paste.png'),'Import File', self)
        self.ctx_actions['import'].triggered.connect(self.importFile)
        self.ctx_actions['del'] = QtWidgets.QAction(QtGui.QIcon(':icons/cross.png'),'Remove', self)
        self.ctx_actions['del'].triggered.connect(self.removeSelected)
        self.ctx_actions['empty'] = QtWidgets.QAction(QtGui.QIcon(':icons/cross.png'),'Empty Trash', self)
        self.ctx_actions['empty'].triggered.connect(self.emptyTrash)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showContextMenu)

    def mouseReleaseEvent(self, evt ):
        ndx = self.indexAt(evt.pos())
        if not ndx.isValid():
            self.clearSelection()
        return QtWidgets.QTreeView.mouseReleaseEvent(self,evt)

    def keyPressEvent( self, evt ):
        if evt.key() == QtCore.Qt.Key_Delete:
            self.removeSelected()
        return QtWidgets.QTreeView.keyReleaseEvent(self,evt)

    def showContextMenu( self, pos ):
        """User context menu
        """
        ndx = self.indexAt(pos)
        if ndx.isValid():
            item = ndx.internalPointer()
            # TODO do different things for trash
            if item.title == 'Trash':
                QtWidgets.QMenu.exec_([self.ctx_actions['empty']], self.mapToGlobal(pos))
            else:
                QtWidgets.QMenu.exec_([self.ctx_actions['new'],
                                       self.ctx_actions['import'],
                                       self.ctx_actions['del']], self.mapToGlobal(pos))

    def emptyTrash( self ):
        """Perma delete
        """
        # TODO write the trash handler
        pass

    def edit( self, index, trigger, evt ):
        """Edit
        """
        if trigger == QtWidgets.QAbstractItemView.DoubleClicked and index.column() != 0:
            return False
        return QtWidgets.QTreeView.edit(self, index, trigger, evt )
            
   
    def openProject( self, fname = None ):
        """Open a project
        """
        self.projPath = None
        if fname is None:
            return
        self.projPath = fname
        pfile = os.path.join(self.projPath,self.PROJ_FILENAME)
        if os.path.exists( pfile ):
            with open(pfile, 'r') as f:
                data = f.read()
            self.model.fromJSON( data )
            self.reset()
       
    def closeProject( self ):
        """Close the project
        """
        self.saveProject()
        self.newProject()
        
    def saveProject( self ):
        """Write the projec file to disk
        """
        if self.projPath is None:
            return
        pfile = os.path.join(self.projPath,self.PROJ_FILENAME)
        data = self.model.toJSON()
        with open( pfile,'w' ) as f:
            f.write( data )

    def importFile( self ):
        """Import a text file into the tree
        """
        ndxs = self.selectedIndexes()
        if len(ndxs) == 0:
            return
        ndx = ndxs[0]

        fname, _ = QtWidgets.QFileDialog.getOpenFileName(self,'Import File','')

        if fname:
            try:
                text = ''
                with open(fname, 'r') as f:
                    text = f.read()
                self.model.insertRow(ndx.row()+1, ndx.parent())
                # grab the newly inserted row
                pndx = self.model.index(ndx.row(), 0, ndx.parent())
                self.setExpanded( pndx, True )
                cndx = self.model.index(self.model.rowCount(pndx)-1,0,pndx)
                citem = self.model.getItem(cndx)
                citem.title = os.path.basename( fname )
                outf = os.path.join(self.projPath,citem.getId() + '.txt')
                with open(outf,'w') as f:
                    f.write( text )
                self.reset()
                self.setExpanded( pndx, True )
            except Exception as inst:
                QtWidgets.QMessageBox.critical(self,'Error', 'Unable to read: %s\n%s' % (fname,str(inst)) )
                return

    def archiveProject( self ):
        """Zip this project up
        """
        if self.projPath is None:
            return
        zipname = os.path.join(os.path.abspath(os.path.join(self.projPath,os.pardir)), self.model._root.title)
        zipname = zipname + '.zip'
        with zipfile.ZipFile( zipname, 'w', zipfile.ZIP_DEFLATED) as zip:
            relroot = os.path.abspath(self.projPath)
            for root, dirs, files in os.walk(self.projPath):
                zip.write(root, os.path.relpath(root,relroot))
                for file in files:
                    filename = os.path.join(root,file)
                    if os.path.isfile(filename):
                        arcname = os.path.join(os.path.relpath(root,relroot), file)
                        zip.write( filename, arcname)
                        

    def addItem( self ):
        """Add a document
        """
        ndxs = self.selectedIndexes()
        if len(ndxs) == 0:
            return None
        ndx = ndxs[0]
                
        self.model.insertRow(ndx.row()+1, ndx.parent())
        # grab the newly inserted row
        pndx = self.model.index(ndx.row(), 0, ndx.parent())
        self.setExpanded( pndx, True )        
        cndx = self.model.index(self.model.rowCount(pndx)-1,0,pndx)        
        self.selectionModel().setCurrentIndex(cndx,QtCore.QItemSelectionModel.ClearAndSelect)
        QtWidgets.QTreeView.edit(self, cndx)

   
    def removeSelected( self ):
        """Move this to the trash
        """
        ndxs = self.selectedIndexes()
        if len(ndxs) == 0:
            return None

        pi = self.model.getItem(ndxs[0].parent())
        if pi is self.model._root:
            return

        ok = QtWidgets.QMessageBox.question(self, 'Confirm',
                                            'Are you sure you want to move this to the trash?')
        if ok == QtWidgets.QMessageBox.Yes:
            item = pi.getChild(ndxs[0].row())
            print( pi.title + ' ' + item.title + ' ' + str(ndxs[0].row()))
            self.model.removeRow(ndxs[0].row()+1, ndxs[0].parent())
            self.model.getTrash().addChild( item )
            # TODO there must be a way to signal a move to trash
            # without having to reset the whole thing
            self.reset()
