from PyQt5 import QtGui, QtCore, QtWidgets

class QColorButton(QtWidgets.QPushButton):
    '''
    Custom Qt Widget to show a chosen color.

    Left-clicking the button shows the color-chooser, while
    right-clicking resets the color to None (no-color).    
    '''

    colorChanged = QtCore.pyqtSignal()

    def __init__(self, *args, **kwargs):
        super(QColorButton, self).__init__(*args, **kwargs)

        self._text = None
        self._color = None
                
        self.setMaximumWidth(32)
        self.pressed.connect(self.onColorPicker)

    def setLabel( self, text ):
        self.setText( text )

    def setColor(self, color):
        '''Set the color of the button
        
        Args:
          color (str): the hex color, e.g '#123abc'
        '''
        if color != self._color:
            self._color = color
            self.colorChanged.emit()

        if self._color:
            c = QtGui.QColor(self._color)
            # http://24ways.org/2010/calculating-color-contrast/
            yiq = ((c.red()*299)+(c.green()*587)+(c.blue()*114))/1000
            if yiq >= 128:
                fg = '#000000'
            else:
                fg = '#FFFFFF'
            self.setStyleSheet(".QColorButton { background-color: %s; color: %s; }" % (self._color,fg))
        else:
            self.setStyleSheet("")

    def color(self):
        return self._color

    def onColorPicker(self):
        '''
        Show color-picker dialog to select color.

        Qt will use the native dialog by default.

        '''
        dlg = QtWidgets.QColorDialog(self)
        if self._color:
            dlg.setCurrentColor(QtGui.QColor(self._color))

        if dlg.exec_():
            self.setColor(dlg.currentColor().name())

    def mousePressEvent(self, e):
        '''Handle right press to reset color to None
        '''
        if e.button() == QtCore.Qt.RightButton:
            self.setColor(None)

        return super(QColorButton, self).mousePressEvent(e)