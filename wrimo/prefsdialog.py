from PyQt5 import QtGui, QtCore, QtWidgets

from .colorbutton import QColorButton

class EditorColorPicker(QtWidgets.QWidget):
    def __init__( self, parent = None ):
        super(EditorColorPicker,self).__init__(parent)
        self.fg = QColorButton()
        self.fg.setLabel('Fg')
        self.bg = QColorButton()
        self.bg.setLabel('Bg')
        self.sample = QtWidgets.QLineEdit("Sample Text")
        
        layout = QtWidgets.QHBoxLayout()
        layout.addWidget( self.fg )
        layout.addWidget( self.bg )
        layout.addWidget( self.sample )

        self.fg.colorChanged.connect( self.onColorChanged )
        self.bg.colorChanged.connect( self.onColorChanged )
        
        self.setLayout( layout )

    def onColorChanged( self ):
        fg = self.fg.color()
        bg = self.bg.color()
        self.sample.setStyleSheet(".QLineEdit { background: %s; color: %s;}" % (bg,fg))


class FontPicker(QtWidgets.QWidget):
    def __init__( self, parent = None ):
        super(FontPicker,self).__init__( parent )
        self.fbutton = QtWidgets.QPushButton('Font', self)
        
        layout = QtWidgets.QHBoxLayout()
        layout.addWidget( self.fbutton )

        self.font = None
        
        self.fbutton.pressed.connect(self.chooseFont)
        
        self.setLayout( layout )

    def chooseFont( self ):
        font, ok = QtWidgets.QFontDialog.getFont()
        if ok:
            self.font = font
            self.fbutton.setFont(font)


class PrefsDialog(QtWidgets.QDialog):
    """This class handles user interactions with app preferences
    """
    def __init__(self, parent):
        super(PrefsDialog,self).__init__( parent )
        self.setWindowTitle("Wrimo Settings")
        #self.resize(600,400)

        self.bBox = QtWidgets.QDialogButtonBox(self)
        self.bBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        
        self.bBox.accepted.connect(self.accept)
        self.bBox.rejected.connect(self.reject)

        self.form = [
            ('Author',        QtWidgets.QLineEdit()),
            ('Author Email',  QtWidgets.QLineEdit()),
            (None,None),
            ('Editor Colors', EditorColorPicker()),            
            ('Editor Font',   FontPicker()),
            ('Notes Colors',  EditorColorPicker()),
            ('Notes Font',    FontPicker()),
        ]

        self.form_lookup = {}
        for item in self.form:
            if item[0] is not None:
                key = item[0].lower().replace(' ', '_')
                #print( item[0] + ' -> ' + key )
                self.form_lookup[key] = item[1]

        layout = QtWidgets.QFormLayout()
        for f in self.form:
            if f[0] is None:
                fr = QtWidgets.QFrame()
                fr.setFrameShape(QtWidgets.QFrame.HLine)
                fr.setFrameShadow(QtWidgets.QFrame.Sunken)
                layout.addRow(fr)
            else:
                layout.addRow(QtWidgets.QLabel(f[0]),f[1])
            
        layout.addRow( self.bBox )

        self.setLayout( layout )

    def getFormWidget( self, key ):
        if key in self.form_lookup:
            return self.form_lookup[key]
        return None


