
from PyQt5 import QtGui, QtCore, QtWidgets

# https://github.com/smathot/quiedit/blob/master/libquiedit/highlighter.py#L44
class MarkdownHighlighter(QtGui.QSyntaxHighlighter):
    def __init__( self, parent = None):
        super(MarkdownHighlighter,self).__init__(parent)
        rules = [
            # header 1
            (u'^#[^#\n]*', 0, self.format(color='#d33682')),
            # header 2
            (u'^##[^#\n]*', 0, self.format(color='#859900')),
            # **bold** and *italic*
            (u'\*(\S[^\*]+\S|[^\*\s]{1,2})\*(?!\w)',0,self.format(italic=True)),
            (u'\*\*(\S[^\*]+\S|[^\*\s]{1,2})\*\*(?!\w)',0,self.format(bold=True)),
            # __bold__ and _italic_
            (u'_(\S[^\*\_]+\S|[^\*\_\s]{1,2})_(?!\w',0,self.format(italic=True)),
            (u'__(\S[^\*\_]+\S|[^\*\_\s]{1,2})__(?!\w)',0,self.format(bold=True)),
            # links
            (u'\[[^@%\]]+\]\(\S+\)', 0, self.format(color='#cb4b16')),
            (u'\[[^@%\]]+\]', 0, self.format(color='#b58900'))
        ]
        
        self.rules = [(QtCore.QRegExp(pat),index,fmt) for(pat,index,fmt) in rules]
        
    def format( self, color=None, bgcolor=None, bold=None, italic=None):
        _fmt = QtGui.QTextCharFormat()

        if color is not None:
            _color = QtGui.QColor()
            _color.setNamedColor(color)
            _fmt.setForeground(_color)

        if bgcolor is not None:
            _bg = QtGui.QColor()
            _bg.setNamedColor(bgcolor)
            _fmt.setBackground(_bg)
            
        if bold is not None:
            if bold:
                _fmt.setFontWeight(QtGui.QFont.Bold)
            
        if italic is not None:
            if italic:
                _fmt.setFontItalic(True)
                
        return _fmt
        
    def highlightBlock( self, text ):
        for exp, nth, fmt in self.rules:
            index = exp.indexIn(text,0)
            while index >= 0:
                index = exp.pos(nth)
                length = len(exp.cap(nth))
                self.setFormat(index,length,fmt)
                index = exp.indexIn(text,index+length)
        #self.setCurrentBlockState(0)
