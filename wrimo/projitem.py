'''
Project Item

@author mtvee
'''
import uuid
import collections

class ProjItem(object):
    """This class is a basic tree to manage tge 
    """
    def __init__(self, title, uid = None ):
        """Initialize a node
        
        Args:
          title(str): a name for this node
          uid(uuid):  a unique id for this node (must be unique over the whole tree)
        """
        self.title = title
        self.id =  uid
        if self.id is None:
            self.id = uuid.uuid4()
        self.__children = list()
        self.__parent = None
        self.__user_meta = collections.OrderedDict()
       
    def __str__( self, *args, **kwargs):
        """return unique repr of this node
        """
        return "{0} ({1})".format(self.title,self.id.hex)
        #return self.data.__str__( *args, **kwargs )

    def __len__( self ):
        """Return thenumber of children
        """
        return len(self.__children)

    def getId( self ):
        """Return the id of this node as a string
        """
        if type(self.id) == uuid.UUID:
            return self.id.hex
        return self.id
    
    def getMetaKeys( self ):
        return list(self.__user_meta.keys())
    
    def setMeta( self, key, value ):
        """So we can keep some user data with this node
        """
        self.__user_meta[key] = value
        
    def getMeta( self, key ):
        """Get user data from this node
        
        Returns:
        User data or an empty string if not found
        """
        if key not in self.__user_meta:
            return ''
        return self.__user_meta[key]

    def toDict( self ):
        """Turn this node into a dict
        """
        d = { 'title' : self.title, 'children' : list(), 'meta' : self.__user_meta }
        d['id'] = self.getId()
        for child in self.__children:
            d['children'].append(child.toDict())
        return d
    
    @classmethod
    def fromDict( cls, d ):
        """Transform a dict into a tree
        """
        if 'title' not in d:
            raise KeyError('Expected \'title\' key')
        if 'id' not in d:
            raise KeyError('Expected \'id\' key')
        if 'children' not in d:
            raise KeyError('Expected \'children\' key')            
        item = ProjItem(d['title'],uuid.UUID(d['id']))
        for child in d['children']:
            item.addChild(cls.fromDict(child))
        if 'meta' in d:
            item.__user_meta = d['meta']
        return item

    def addChild( self, child ):
        """Add a child node to this node
        """
        if isinstance(child,ProjItem):
            self.__children.append(child)
            child.__parent = self
            return child
        else:
            raise TypeError('Child of ProjNode should be ProjNode: ' + type(child))

    def getParent( self ):
        """Return this nodes parent
        """
        return self.__parent
    
    def getChild( self, index ):
        """return child at index
        
        this will throw an IndexError
        """
        return self.__children[index]

    def getRow( self ):
        """Return our row number in the parent list
        """
        i = 0
        if self.__parent is None:
            return 0;
        for c in self.__parent.__children:
            if c.id == self.id:
                return i
            i = i + 1
        return -1
            

    def moveChild( self, item, parent, index = -1):
        p1 = item.__parent;
        if p1 is None:
            return
        p1.__children.remove(item)
        if index != -1:
            parent.__children.insert(index,item)
        else:
            parent.__children.append(item)
        item.__parent = parent

    def addChildren( self, kids ):
        if isinstance(kids,list):
            for kid in kids:
                if isinstance(kid,ProjItem):
                    self.__children.append(kid)
                    kid.__parent = self
                else:
                    raise TypeError('Child of ProjItem should be ProjItem')

    def getChildren( self ):
        """return a list of all children
        """
        return self.__children

    def findItems( self, title ):
        """Find item(s) with title
        
        Titles are not unique so this can return more than one item
        """
        nodes = []
        rnodes = []
        
        nodes.extend(self.getChildren())
            
        while nodes:
            child = nodes[0]
            if child.title == title:
                rnodes.append( child )
                del nodes[0]
            else:
                nodes.extend(child.getChildren())
                del nodes[0]
        
        return rnodes
        

    def getItem( self, uid, includeself=True):
        """find item with uid
        
        TODO make this make sense
        """
        nodes = []
        if incluseself:
            nodes.append(self)
        else:
            nodes.extend(self.getChildren())
            
        while nodes:
            child = nodes[0]
            if child.id == uid:
                return child
            else:
                nodes.extend(child.getChildren())
                del nodes[0]

    def delChild( self, index ):
        """Remove child at index
        
        this will throw an index error
        """
        del self.__children[index]
         
    def delItem( self, uid ):
        """ fix this so we delete ourselves from the
            parent list and use find to get the nodes
        """
        nodes = [self]
        while nodes:
            child = nodes[0]
            if child.id == uid:
                if child.isRoot():
                    del self
                    return
                else:
                    parent = child.getParent()
                    parent.delChild(parent.getChildren().index(child))
                    return
            else:
                nodes.extend(child.getChildren())
                del nodes[0]
    
    def getPath( self, sep='.' ):
        """return a string path to the node
        """
        path = [self.title]
        p = self.getParent()
        while p:
            path.append(p.title)
            p = p.getParent()
        path.reverse()
        return sep.join(path)
    
    def getRoot( self ):
        """return the ultimate root of this node
        """
        if self.isRoot():
            return self
        else:
            return self.getParent().getRoot()

    def isRoot( self ):
        """this is a root node is it has no parent
        """
        if self.__parent is None:
            return True
        else:
            return False

    def isBranch(self):
        """this is a leaf node if it has no children
        """
        if self.__children == []:
            return True
        else:
            return False

    def printPretty(self):
        """Pretty printer
        """
        level = 0
        nodes = [self,level]
        
        while nodes:
            head = nodes.pop()
            if isinstance(head,int):
                level = head
            else:
                self._printLabel(head,nodes,level)
                children = head.getChildren()
                children.reverse()
                
                if nodes:
                    nodes.append(level)
                    
                if children:
                    nodes.extend(children)
                    level += 1
                    nodes.append(level)

    def _printLabel( self, head, nodes, level ):
        """used by pretty printer to indent
        """
        leading = ''
        lasting = '|___ '
        label = str(head)
        
        if level == 0:
            print( str(head) )
        else:
            for x in range(0, level-1 ):
                sibling = False
                pt = head._getParent(level - x)
                for c in pt.getChildren():
                    if c in nodes:
                        sibling = True
                        break
                if sibling:
                    leading += '|    '
                else:
                    leading += '    '
                        
            if label.strip() != '':
                print('{0}{1}{2}'.format(leading,lasting,label))

    def _getParent( self, up ):
        """return up'th parent node
        """
        parent = self
        while up:
            parent = parent.getParent()
            up -= 1
        return parent

    