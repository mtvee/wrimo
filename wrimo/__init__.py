import sys
from .wrimoapp import WrimoApp
from .version import __version__

def main():
    wapp = WrimoApp(sys.argv)
    wapp.exec_()

__all__ = ('main','WrimoApp')
