import os
import collections
from PyQt5 import QtGui, QtCore, QtWidgets, QtWebKitWidgets

from .version import __version__
from .projview import ProjectTree
from .findreplace import FindReplace
from .prefsdialog import PrefsDialog
from .webbrowser import WebBrowser
from .syntax.markdown import MarkdownHighlighter
from .resources_rc import *


class MainWindow(QtWidgets.QMainWindow):
    """Main class of hte Wrimo application
    """
    def __init__(self):
        """Init the class
        """
        super(MainWindow,self).__init__()
        
        self.setWindowIcon(QtGui.QIcon(':icons/icon.png'))

        # the place we keep all our projects
        self.baseProjectPath = ''
        # the name of the current project
        self.curProject = None
        # the name of the current document
        self.curDocument = None
        self.curDocumentDirty = False
        
        # figure out where we are for config stuffs
        paths = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.ConfigLocation)
        if not os.path.exists(paths[0]):
            self.firstTimeSetup( paths[0] )
                
        # splitters
        self.split = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.vsplit = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        
        # tabs for various infos
        self.tabs = QtWidgets.QTabWidget(self)
        # notes, actually
        self.details = QtWidgets.QTextEdit(self)
        # key,values from ProjItem.get/setMeta
        self.userTable = QtWidgets.QTableWidget(self)
        self.userTable.setColumnCount(2)
        self.userTable.setHorizontalHeaderLabels(['Item','Data'])
        self.userTable.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.userTable.customContextMenuRequested.connect(self.userTableContextMenu)
        self.userTable.verticalHeader().hide()
        
        self.ut_ctx_actions = {}
        self.ut_ctx_actions['add'] = QtWidgets.QAction(QtGui.QIcon(':icons/document-new.png'),'Add row', self)
        self.ut_ctx_actions['add'].triggered.connect(self.addUserTableRow)
        self.ut_ctx_actions['del'] = QtWidgets.QAction(QtGui.QIcon(':icons/cross.png'),'Remove row', self)
        self.ut_ctx_actions['del'].triggered.connect(self.delUserTableRow)

        
        self.tabs.addTab(self.details,"Notes")
        self.tabs.addTab(self.userTable,"Data")
        
        # the project tree
        self.projView = ProjectTree(self)
        # the main editor
        self.editor = QtWidgets.QTextEdit(self)
        # this marks the document dirty for some reason
        MarkdownHighlighter(self.editor)
        
        self.vsplit.addWidget( self.projView )
        self.vsplit.addWidget( self.tabs )
        self.split.addWidget( self.vsplit )
        self.split.addWidget( self.editor )
        
        self.setCentralWidget(self.split)
        
        self.createActions()
        self.createMenus()
        self.createToolbars()
        #self.setStyle()
        
        self.readSettings()
        
        # -----------------
        # statusbar widgets
        # -----------------
        # word count/target
        self.targetWordCount = 0
        self.wordCount = QtWidgets.QPushButton(self)
        self.wordCount.setText("0/0")
        self.wordCount.setFlat(True)
        self.wordCount.setToolTip("Click to set a word count target")
        self.statusBar().addPermanentWidget( self.wordCount )
        self.wordCount.clicked.connect( self.setWordCount )
        
        # sprint target
        self.timer = None
        self.timerWordCount = 0
        self.timerCount = QtWidgets.QPushButton( self )
        self.timerCount.setText("0:00")
        self.timerCount.setFlat(True)
        self.timerCount.setToolTip("Click to set a word sprint")
        self.statusBar().addPermanentWidget( self.timerCount )
        self.timerCount.clicked.connect( self.setTimerCount )
                
        # wiring
        # ------
        #self.editor.document().contentsChanged.connect(self.docChanged)
        self.editor.textChanged.connect(self.docChanged)
        self.projView.selectionModel().selectionChanged.connect(self.projSelChanged)
        self.projView.doubleClicked.connect(self.projItemDoubleClicked)
        
        self.setWindowTitle("Wrimo v%s" % __version__)
        self.statusBar().showMessage("Ready to rock")
                
        self.editor.setFocus()
        
    def firstTimeSetup( self, path ):
        """Setup the path and defaults for the editor
        """
        pass
        
    def setStyle( self ):
        """Set the UI style
        """
        if os.path.exists('assets/styles/style_dark_orange.css'):
            with open('assets/styles/style_dark_orange.css','r') as f:
                css = f.read()
            self.setStyleSheet(css)

    # table stuff        
    def addUserTableRow( self ):
        self.userTable.insertRow(self.userTable.rowCount())

    def delUserTableRow( self ):
        sel = self.userTable.selectedItems()
        if len(sel) == 0:
            return
        
        for item in sel:
            print(item.row())
            self.userTable.removeRow(item.row()-1)
        
    def userTableContextMenu( self, pos ):
        QtWidgets.QMenu.exec_([self.ut_ctx_actions['add'],
                               self.ut_ctx_actions['del']], self.userTable.mapToGlobal(pos))
        
    def loadUserTable( self, item ):
        self.userTable.setRowCount(0)
        i = 0
        for key in item.getMetaKeys():
            if key != 'note':
                self.userTable.insertRow(self.userTable.rowCount())
                self.userTable.setItem(i, 0, QtWidgets.QTableWidgetItem( key ))
                self.userTable.setItem(i, 1, QtWidgets.QTableWidgetItem( item.getMeta(key) ))
                i += 1
    
    def saveUserTable( self, item ):
        for i in range(self.userTable.rowCount()):
            key = self.userTable.item( i, 0 )
            if key != 'note':
                val = self.userTable.item( i, 1 )
                item.setMeta(key.text(), val.text())
    
    # /table stuff
        
    def projSelChanged( self, selto, selfrom ):
        """Called when the selection in the ProjectView changes
        
        Args:
          selto (QSelection): the currently selected item(s)
          selfrom (QSelection): the previously selected item(s)
        """
        newitems = selto.indexes()
        olditems = selfrom.indexes()
        
        oldnote = self.details.toPlainText()
        if len(olditems):
            item = self.projView.model.getItem(olditems[0])
            item.setMeta('note',oldnote)
            self.saveUserTable( item )

        self.details.clear()
        if len(newitems):
            item = self.projView.model.getItem(newitems[0])
            self.details.setText(item.getMeta('note'))
            self.loadUserTable( item )

    def projItemDoubleClicked(self, ndx ):
        """ Called when a ProjectView item is double clicked
        
        Args:
          item (QTreeViewItem): the clicked item
          column (int): the column that was clicked
        """
        if not ndx.isValid():
            return
        
        if self.baseProjectPath is None or self.curProject is None:
            return

        if self.curDocument is not None:
            self.saveAction(None)
        
        item = self.projView.model.getItem( ndx )
        fname = os.path.normpath(os.path.join(self.baseProjectPath,self.curProject,item.getId() + '.txt'))
        text = ''
        if os.access( fname, os.W_OK ):
            with open( fname, 'r' ) as f:
                text = f.read()
        self.editor.clear()
        self.editor.setText(text)
        self.editor.textCursor().movePosition(QtGui.QTextCursor.Start)
        self.editor.ensureCursorVisible()
        self.editor.setFocus()
        self.curDocumentDirty = False
        self.curDocument = fname
        
    def setWordCount( self ):
        """Called to allow the user to set the word count goal
        """
        cnt, ok = QtWidgets.QInputDialog.getInt(self,'Set Target','Word count',1,1,20000,1)
        if ok:
            self.targetWordCount = cnt
    
    def setTimerCount( self ):
        """Called to allow the user to set the timer
        
        Note:
          returns immediately if there is already a timer running
        """
        if self.timer is not None:
            return
        
        cnt, ok = QtWidgets.QInputDialog.getInt(self,'Set Timer','Minutes',5,1,120,1)
        if ok:
            self.timerWordCount = len(str(self.editor.toPlainText()).split())
            self.timerValue = cnt * 60
            self.timerMins = cnt
            self.timer = QtCore.QTimer(self)
            self.timer.timeout.connect(self.updateTimer)
            mins = self.timerValue / 60
            secs = self.timerValue % 60
            self.timerCount.setText("%d:%02d" % (mins,secs))
            self.timer.start(1000)
        
    def updateTimer( self ):
        """Called by the system when a timer tick happens
        """
        if self.timer is None:
            return
        
        self.timerValue -= 1;
        if self.timerValue <= 0:
            self.timer.stop()
            self.timer = None
            wc = len(str(self.editor.toPlainText()).split())
            self.timerCount.setText("0:00")
            wc = wc - self.timerWordCount
            QtWidgets.QMessageBox.information(self,
                                              "Time's up",
                                              "Your word count: %d (%d wpm)" % (wc,wc/self.timerMins))
            self.timerValue = 0
            self.timerWordCount = 0
            self.timerMins = 0

        else:
            mins = self.timerValue / 60
            secs = self.timerValue % 60
            self.timerCount.setText("%d:%02d" % (mins,secs))
        
    def docChanged( self ):
        """Called when the editor document changes
        """
        self.curDocumentDirty = True
        # TODO this would be better as a fade in/out message
        # so don't have to break stride to hit the dialog box
        wc = len(str(self.editor.toPlainText()).split())
        clen = len(str(self.editor.toPlainText()))
        
        if self.targetWordCount > 0 and self.targetWordCount <= wc:
            QtWidgets.QMessageBox.information(self,
                                              'Boom!',
                                              'You hit your word count: %d' % (self.targetWordCount))
            self.targetWordCount = 0
            
        if self.targetWordCount > 0 and wc < self.targetWordCount:
            wc = self.targetWordCount - wc
            
        self.wordCount.setText("%d/%d" % (wc,clen))
        
    def newProjectAction( self, evt ):
        """Called by user to create a new project
        """
        fname, _ = QtWidgets.QFileDialog.getSaveFileName(self,'New Project',
                                                  self.baseProjectPath)
        if fname:
            fname = str(fname)

            if os.path.exists(fname):
                # TODO ask is we want to open it
                QtWidgets.QMessageBox.critical(self,'Error', 'Already exists: %s' % fname )
                return
            try:
                os.mkdir( fname )
                #os.mkdir( os.path.join(fname,'Drafts'))
                #os.mkdir( os.path.join(fname,'Resources'))
                #os.mkdir( os.path.join(fname,'Trash'))
            except Exception as inst:
                QtWidgets.QMessageBox.critical(self,'Error', 'Unable to create: %s\n%s' % (fname,str(inst)) )
                return
            self.baseProjectPath = os.path.dirname(fname)
            self.curProject = fname
            self.setWindowTitle("Wrimo v%s: %s" % (__version__,os.path.basename(fname)))
            self.projView.openProject( self.curProject )
            self.projView.model.newProject()
            self.projView.reset()

    def newDocumentAction( self, evt ):
        """Called by user to create a new document
        """
        self.projView.addItem()
        
    def openProjectAction( self, evt ):
        """Called by user to open an existing project
        """
        fname = QtWidgets.QFileDialog.getExistingDirectory(self,
                                                           'Open Project',
                                                           self.baseProjectPath)
        if fname:
            print(fname)
            if self.curProject is not None:
                self.projView.saveProject()
            if self.curDocument is not None:
                self.saveAction(evt)
            self.baseProjectPath = os.path.dirname(fname)
            self.curProject = fname
            self.setWindowTitle("Wrimo v%s: %s" % (__version__,os.path.basename(fname)))
            self.projView.openProject( self.curProject )
            self.details.setText('')
            self.editor.setText('')
            self.curDocumentDirty = False
               
    def saveAction( self, evt ):
        """Called by user or system to save the current document
        """
        if self.curDocument is None:
            return self.saveAsAction(evt)
        try:
            with open(self.curDocument,'w') as f:
                f.write(self.editor.toPlainText())
            return True
        except:
            QtWidgets.QMessageBox.critical(self,
                                           'Unable to save',
                                           'Could not write to: %s' % self.curDocument )
            return False
    
    def saveAsAction( self, evt ):
        """Called by user or system to save an unnamed document
        
        Returns:
          bool: True if file saved, False on error or cancel
        """
        fname,_ = QtWidgets.QFileDialog.getSaveFileName(self,
                                                        'Save File',
                                                        self.baseProjectPath)
        if fname:
            try:
                with open(fname,'w') as f:
                    f.write(self.editor.toPlainText())
                self.curDocument = fname
                return True
            except:
                QtWidgets.QMessageBox.critical(self,'Unable to save',
                                               'Could not write to: %s' % fname )
        return False

    def prefsAction(self, evt ):
        """Called by user to manage settings
        """
        dlg = PrefsDialog( self )
        ok = dlg.exec_()
        if ok == QtWidgets.QDialog.Accepted:
            # TODO fix this uglyness
            font = dlg.getFormWidget('editor_font')
            if font is not None:
                self.editor.setFont( font.font )
    
    def renderAction( self, evt ):
        dlg = WebBrowser(self, 'Wrimo Render Preview')
        dlg.renderText(self.editor.toPlainText())
        dlg.exec_()

    def helpAction( self, evt ):
        """Called by user for help
        """
        # TODO add help
        dlg = WebBrowser(self, 'Wrimo Help')
        dlg.setTopic('index')
        dlg.exec_()
    
    def aboutAction( self, evt ):
        """Called by user to show about information
        """
        QtWidgets.QMessageBox.about(self, "About Wrimo",
                                    "This is Wrimo v%s\n\n"
                                    "http://bitbucket.org/mtvee/wrimo" % (__version__))

    def createActions(self):
        """Called by system to create all actions
        
        Note:
          actions are stored in a dictionary
        """
        self.actions = {}
        # File Menu
        act = QtWidgets.QAction(QtGui.QIcon(':icons/book.png'),
                            'New Project', self)
        act.triggered.connect( self.newProjectAction )
        self.actions['newproj'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/document-new.png'),
                            'New Document', self)
        act.triggered.connect( self.newDocumentAction )
        self.actions['newdoc'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/book.png'),
                            'Open Project...', self)
        act.triggered.connect( self.openProjectAction )
        self.actions['openproj'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/document-save.png'),
                            'Save', self)
        act.setShortcut("Ctrl+S")
        act.triggered.connect( self.saveAction )
        self.actions['save'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/document-save-as.png'),
                            'Save as...', self)
        act.triggered.connect( self.saveAsAction )
        self.actions['saveas'] = act
        
        act = QtWidgets.QAction('Quit Wrimo', self)
        act.setShortcut("Ctrl+Q")
        act.triggered.connect( self.close )
        self.actions['quit'] = act

        # Edit Menu
        act = QtWidgets.QAction(QtGui.QIcon(':icons/edit-undo.png'),
                            'Undo', self)
        act.setShortcut(QtGui.QKeySequence.Undo)
        act.triggered.connect( self.editor.undo )
        self.actions['undo'] = act
        
        act = QtWidgets.QAction(QtGui.QIcon(':icons/edit-redo.png'),
                            'Redo', self)
        act.setShortcut(QtGui.QKeySequence.Redo)
        act.triggered.connect( self.editor.redo )
        self.actions['redo'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/edit-cut.png'),
                            'Cut', self)
        act.setShortcut(QtGui.QKeySequence.Cut)
        act.triggered.connect( self.editor.cut )
        self.actions['cut'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/edit-copy.png'),
                            'Copy', self)
        act.setShortcut(QtGui.QKeySequence.Copy)
        act.triggered.connect( self.editor.copy )
        self.actions['copy'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/edit-paste.png'),
                            'Paste', self)
        act.setShortcut(QtGui.QKeySequence.Paste)
        act.triggered.connect( self.editor.paste )
        self.actions['paste'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/edit-find-replace.png'),
                            'Find', self)
        act.setShortcut("Ctrl+F")
        act.triggered.connect( FindReplace(self,self.editor).show )
        self.actions['find'] = act

        # Tools Menu
        act = QtWidgets.QAction(QtGui.QIcon(':icons/doc/html.png'),
                                'Render...', self)
        act.setShortcut("F2")
        act.triggered.connect( self.renderAction )
        self.actions['render'] = act

        act = QtWidgets.QAction(QtGui.QIcon(':icons/doc/zip.png'),
                                'Archive project', self)
        #act.setShortcut("F2")
        act.triggered.connect( self.projView.archiveProject )
        self.actions['archive'] = act
        
        act = QtWidgets.QAction(QtGui.QIcon(':icons/prefs-general.png'),
                                'Options...', self)
        act.triggered.connect( self.prefsAction )
        self.actions['prefs'] = act
        
        # Help Menu
        act = QtWidgets.QAction(QtGui.QIcon(':icons/help-contents.png'),
                                'Help', self)
        act.setShortcut("F1")
        act.triggered.connect( self.helpAction )
        self.actions['help'] = act
        
        act = QtWidgets.QAction('About Wrimo', self)
        act.triggered.connect( self.aboutAction )
        self.actions['about'] = act
        

    def createMenus( self ):
        """Called by system to create window menus
        """
        fm = self.menuBar().addMenu("&File")
        newmenu = fm.addMenu("New...")
        newmenu.addAction(self.actions['newproj'])
        newmenu.addAction(self.actions['newdoc'])
        fm.addAction(self.actions['openproj'])
        fm.addSeparator()
        fm.addAction( self.actions['save'])
        fm.addAction( self.actions['saveas'])
        fm.addSeparator()
        fm.addAction( self.actions['quit'])
        
        em = self.menuBar().addMenu("&Edit")
        em.addAction(self.actions['undo'])
        em.addAction(self.actions['redo'])
        em.addSeparator()
        em.addAction(self.actions['cut'])
        em.addAction(self.actions['copy'])
        em.addAction(self.actions['paste'])
        em.addSeparator()
        em.addAction(self.actions['find'])

        tm = self.menuBar().addMenu("&Tools")
        tm.addAction( self.actions['render'])
        tm.addAction( self.actions['archive'])
        tm.addSeparator()
        tm.addAction( self.actions['prefs'])
        
        hm = self.menuBar().addMenu("&Help")
        hm.addAction( self.actions['help'])
        hm.addAction( self.actions['about'])

        
    def createToolbars(self):
        """Called by system to create toolbar
        """
        ft = self.addToolBar("File")
        ft.addAction(self.actions['newproj'])
        ft.addAction(self.actions['newdoc'])
        ft.addSeparator()
        
    def closeEvent( self, evt ):
        """Called by system when window wants to close
        """
        if self.curDocumentDirty:
            ret = QtWidgets.QMessageBox.warning(self, "Not saved",
                                                "The current document has been modified but not saved\nDo you want to save your changes?",
                                                QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.Cancel)
            if ret == QtWidgets.QMessageBox.Save:
                if self.curDocument is not None:
                    self.saveAction(None)
                elif not self.saveAsAction(evt):
                    evt.ignore()
                    return
            elif ret == QtWidgets.QMessageBox.Cancel:
                evt.ignore()
                return
        self.projView.saveProject()
        self.writeSettings()
        evt.accept()
   
    def readSettings( self ):
        """Called by system to read application settings
        """
        s = QtCore.QSettings()
        self.move(s.value("pos", QtCore.QPoint(100,100),type=QtCore.QPoint))
        self.resize(s.value("size", QtCore.QSize(600,400),type=QtCore.QSize))
        if s.contains("split"):
            self.split.restoreState(s.value("split"))

        # editor colors
        #pal = self.editor.palette()
        #pal.setColor(QtGui.QPalette.Base, QtGui.QColor(s.value("background","#002b36")))
        #pal.setColor(QtGui.QPalette.Text, QtGui.QColor(s.value("foreground","#fdf6e3")))
        #self.editor.setPalette(pal)
        css = '.QTextEdit {background-color: %s; color: %s;}' % (s.value("background","#002b36"),s.value("foreground","#fdf6e3"))
        self.editor.setStyleSheet( css )
        
        # font
        self.editor.setFont(QtGui.QFont(s.value("fontfamily","Courier"),
                                        s.value("fontsize","12",type=int)))
        
        self.editor.setLineWrapMode(QtWidgets.QTextEdit.FixedColumnWidth)
        self.editor.setLineWrapColumnOrWidth(s.value("wrapcolumn","80",type=int))
        self.editor.setWordWrapMode( QtGui.QTextOption.WordWrap )
        
        # details/notes colors
        pal = self.details.palette()
        pal.setColor(QtGui.QPalette.Base, QtGui.QColor(s.value("note-background","#eee8d5")))
        pal.setColor(QtGui.QPalette.Text, QtGui.QColor(s.value("note-foreground","#073642")))
        self.details.setPalette(pal)
        self.details.setFontPointSize(s.value("note-fontsize","11",type=int))
        
        self.baseProjectPath = str(s.value("basepath",''))
   
    def writeSettings( self ):
        """Called by system to save application settings
        """
        s = QtCore.QSettings()
        s.setValue("pos",        self.pos())
        s.setValue("size",       self.size())
        s.setValue("split",      self.split.saveState())
        s.setValue("basepath",   self.baseProjectPath )
        f = self.editor.currentFont()
        s.setValue("fontfamily", f.family())
        s.setValue("fontsize",   f.pointSize())
        