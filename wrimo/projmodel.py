from PyQt5 import QtGui, QtCore, QtWidgets
import collections
import json

from .projitem import ProjItem

class ProjModel(QtCore.QAbstractItemModel):
    def __init__(self, title, path):
        super(ProjModel,self).__init__()
        
        # the data
        self._root = ProjItem(title)
        
        self.base_path = path
        self._headings = ['Name','W','C']
        self.labels = ['Idea','Note','Character','Chapter','Scene']
        self.statuses = ['ToDo','First Draft','Revised Draft','Final Draft','Done']
        
    def index( self, row, column, parentIndex ):
        if not self.hasIndex( row, column, parentIndex):
            return QtCore.QModelIndex()
        pitem = self.getItem( parentIndex )
        citem = pitem.getChild( row )
        return self.createIndex( row, column, citem )

    def newProject( self ):
        self._root.addChild(ProjItem('Drafts'))
        self._root.addChild(ProjItem('Resources'))
        self._root.addChild(ProjItem('Trash'))
            
    def hasIndex( self, row, column, parentIndex ):
        if column < 0 or column > len(self._headings) or row < 0:
            return False
        pitem = self.getItem(parentIndex)
        if row >= len(pitem):
            return False
        return True
    
    def parent( self, childIndex ):
        if not childIndex.isValid():
            return QtCore.QModelIndex()
        item = childIndex.internalPointer()
        if item.getParent() is None or item.getParent() is self._root:
            return QtCore.QModelIndex()
        else:
            return self.createIndex( item.getParent().getRow(), 0, item.getParent())
        
        parent = item.getParent()
        if parent == self._root:
            return QtCore.QModelIndex()
        else:
            return self.createIndex(parent.getRow(),0,parent)
        
    def hasChildren( self, index ):
        if not index.isValid():
            item = self._root
        else:
            item = index.internalPointer()
        return len(item) > 0
        
    def rowCount( self, parentIndex ):
        if parentIndex.isValid():
            item = parentIndex.internalPointer()
        else:
            item = self._root
        return len(item)
    
    def columnCount( self, parentIndex ):
        return len(self._headings)
    
    def data( self, index, role ):
        if index.isValid():
            if role == QtCore.Qt.DisplayRole:
                item = index.internalPointer()
                if index.column() == 0:
                    return item.title
                else:
                    return '0'
            elif role == QtCore.Qt.DecorationRole:
                if index.column() == 0:
                    item = self.getItem(index)
                    if item.getParent() == self._root:
                        if item.title == 'Trash':
                            return QtGui.QIcon(':icons/trash.png')
                        else:
                            return QtGui.QIcon(':icons/folder-open.png')
                    if len(item):
                        return QtGui.QIcon(':icons/folder-open.png')
                    else:
                        return QtGui.QIcon(':icons/document-new.png')
                
        return QtCore.QVariant()
    
    def setData( self, index, value, role ):
        if index.isValid() and role == QtCore.Qt.EditRole:
            item = index.internalPointer()
            if index.column() == 0:
                item.title = value
                return True
        return False
    
    def flags( self, index ):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        if index.column() != 0:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        item = index.internalPointer()
        # can't edit the trash
        if item.title == 'Trash':
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        if item.getParent() is self._root:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDropEnabled
        return  QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled
    
    def headerData( self, column, orientation, role ):
        if role == QtCore.Qt.DisplayRole:
            return self._headings[column]
        
    def insertRows( self, pos, rows, parentIndex ):
        pi = self.getItem( parentIndex )
        self.beginInsertRows( self.index(pos-1,0,parentIndex), pos, pos+rows-1)
        pi.getChild(pos-1).addChild( ProjItem('New Doc'))
        self.endInsertRows()
        return True
        
    def removeRows( self, pos, rows, parentIndex ):
        pi = self.getItem(parentIndex)
        # make sure top-level folders are not removed
        if pi.getChild(pos-1).getParent() is self._root:
            return False
        
        self.beginRemoveRows(parentIndex,pos-1,pos+rows-2)
        item = pi.getChild(pos-1)
        pi.delChild(pos-1)
        self.endRemoveRows()
        return True
    
    # DnD
    def supportedDropActions(self):
        return QtCore.Qt.MoveAction
        
    def mimeTypes( self ):
        return ['text/plain']
        
    def mimeData( self, ndxs ):
        if not len(ndxs):
            return 0
        data = []
        for ndx in ndxs:
            if ndx.isValid():
                item = ndx.internalPointer()
                data.append(json.dumps(item.toDict()))
        md = QtCore.QMimeData()
        md.setData('text/plain', data[0])
        return md
    
    def dropMimeData( self, data, action, row, column, parent ):
        dic = json.loads(bytes(data.data('text/plain')).decode('utf-8'))
        pitem = self.getItem( parent )
        print( '%s %d %d %s' % (str(dic),row,column,pitem.title))
        pitem.addChild( ProjItem.fromDict(dic) )
        return True
    # /Dnd

    def getItem( self, ndx ):
        if ndx.isValid():
            item = ndx.internalPointer()
            return item
        return self._root

    def getTrash( self ):
        trash = self._root.findItems('Trash')
        assert len(trash) == 1
        return trash[0]
    
    def toJSON( self ):
        return json.dumps(self._root.toDict())
    
    def fromJSON( self, string ):
        self._root = ProjItem.fromDict(json.loads(string,object_pairs_hook=collections.OrderedDict))
