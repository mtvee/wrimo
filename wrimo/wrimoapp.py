
from PyQt5 import QtCore, QtGui, QtWidgets
from .mainwindow import MainWindow
from .version import __version__

class WrimoApp(QtWidgets.QApplication):
    """Application class
    """
    def __init__( self, argv ):
        """Init
        
        Args:
          argv (list): command line arguments
        """
        super(WrimoApp,self).__init__( argv )
        self.setOrganizationName('j2mfk')
        self.setApplicationName('wrimo')
        self.setApplicationVersion(__version__)
        self.mainwin = MainWindow()
        self.mainwin.show()
