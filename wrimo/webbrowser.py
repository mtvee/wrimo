from PyQt5 import QtGui, QtCore, QtWidgets, QtWebKitWidgets

from .parsers import mistune

class WebBrowser(QtWidgets.QDialog):
    """Simple web browser
    """
    def __init__( self, parent, title ):
        super(WebBrowser,self).__init__( parent )
        
        self.setWindowTitle(title)
        self.setMinimumWidth( 600 )
        self.setMinimumHeight( 400 )
        
        layout = QtWidgets.QVBoxLayout()
        self.setLayout( layout )
        
        self.browser = QtWebKitWidgets.QWebView()
        layout.addWidget( self.browser )

    def setTopic( self, topic ):
        """load a help topic
        """
        url = "qrc:///help/" +  topic + '.htm'
        print(url)
        self.browser.load(QtCore.QUrl(url))

    def renderText( self, text ):
        head = """<!doctype html>
        <html>
        <style>
        * {
            font-family: sans-serif;
        }
        </style>
        <head></head><body>
        """
        html = head + mistune.markdown(text)
        self.browser.setHtml( html )
