import os
from fabric.api import env, task, run, local, put, cd

# setup your deploy host stuff, if any
#env.use_ssh_config = True
#env.hosts = ['j2']

# clean out pyc files
@task
def clean():
    for f in [os.path.join(r,f) for r,d,fs in os.walk('.') for f in fs if f.endswith('.pyc')]:
        os.remove(f)

